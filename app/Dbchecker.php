<?php
namespace app;

use Zippopotamus\Service\Zippopotamus;

class Dbchecker
{

    public $country;
    public $zipcode;
    public function __construct($country,$zipcode)
    {
        $this->country = $country;
        $this->zipcode = $zipcode;
    }
    public function CheckData(){
        $servername = "localhost";
        $username = "root";
        $password = "";
        $dbname = "wic";

        $conn = new \mysqli($servername, $username, $password, $dbname);

        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }
        $sql = "SELECT country, zipcode, city,state FROM wic WHERE zipcode=$this->zipcode";
        $result = $conn->query($sql);

        if ($result->num_rows > 0) {
            // output data of each row
            echo "Data already exist in database </br>";
            while($row = $result->fetch_assoc()) {
                echo "country: " . $row["country"]. " - zipcode: " . $row["zipcode"]. " city " . $row["city"]." state ".$row["state"]."<br>";
            }
            return true;
        } else {
            $result = Zippopotamus::code($this->country, $this->zipcode);
            if($result != null){
                $city = $result->places[0]->{'place name'};
                $state = $result->places[0]->state;

                $zipcode = (int) $this->zipcode;

                $sql = "INSERT INTO wic(country,zipcode,city,state) VALUES ('$this->country', '$zipcode', '$city','$state')";
                if ($conn->query($sql) === TRUE) {
                    echo "New record created successfully";
                } else {
                    echo "Error: " . $sql . "<br>" . $conn->error;
                }
                return false;
            }else{
                echo "Our database is clear sorry";
            }

        }
        $conn->close();
    }
}